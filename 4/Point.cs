struct Point
{
    public Point(double x, double y)
    {
        X=x;
        Y=y;
    }
    public double X;
    public double Y;
    public override bool Equals( object ob )
    {
		if( ob is Point ) 
        {
			Point p = (Point) ob;
			return X== p.X && Y ==p.Y;
		}
		else 
        {
			return false;
		}
	}
    public override int GetHashCode()
    {
		return X.GetHashCode() * Y.GetHashCode();
	}
}