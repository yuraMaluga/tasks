using System;
using System.Windows;

class Task4
{
    class Rectangle
    {
       #region Fields
        public Point FirstPoint{get;set;} 
        public double Height{get;set;}
        public double Width{get;set;}
       #endregion

       #region Constructors
           public Rectangle( Point a, double height,double weight)
            {
                Height =height;
                Width =weight;
                FirstPoint=a;
            }
       #endregion

       #region Methods

        public Rectangle( Point a, double height,double weight)
        {
            Height =height;
            Width =weight;
            FirstPoint=a;
        }

        public void MoveAt(Point newFirstPoint)
        {
            FirstPoint= newFirstPoint;
        }

        public Point[] GetPointsArray()
        {
           return new Point[]
           {
               new Point(FirstPoint.X,FirstPoint.Y),
               new Point(FirstPoint.X,FirstPoint.Y+Height),
               new Point(FirstPoint.X+Width,FirstPoint.Y+Height),
               new Point(FirstPoint.X+Width,FirstPoint.Y)
           };
        }

        public void Print()
        {
            Console.WriteLine("Rectangle");
            Console.WriteLine("Height: {0}, Width: {1}.",Height,Width);
            Console.Write("Points: ");
            foreach (var item in GetPointsArray())
            {
                Console.Write(" [{0}; {1}] , ",item.X,item.Y);
            }
            Console.WriteLine();
        }
       
       // find the minimal rectangle which contain this and rec2
        public Rectangle MinRectangle (Rectangle rec2)
        {
           double newX = Math.Min(FirstPoint.X,rec2.FirstPoint.X);
           double newY = Math.Min(FirstPoint.Y,rec2.FirstPoint.Y);
           double width = Math.Max(FirstPoint.X+Width, rec2.FirstPoint.X+rec2.Width) - newX;
           double height = Math.Max(FirstPoint.Y+Height, rec2.FirstPoint.Y+rec2.Height) - newY;
           return new Rectangle(new Point(newX,newY),height,width);
        }

        public Rectangle Crossing(Rectangle rec)
        {
            Point crossingPoint;
           if(IsCrossingPoint(rec,out crossingPoint))
           {
              Point crossingThisPoint;
              rec.IsCrossingPoint(this,out crossingThisPoint);
              if(crossingPoint.Equals(crossingThisPoint))
              {
                  Console.WriteLine("These rectangles have common only point [{0}:{1}]",crossingPoint.X,crossingPoint.Y);
                  return new Rectangle(crossingPoint,0,0);
              }
              if((crossingPoint.X == crossingThisPoint.X)||(crossingPoint.Y == crossingThisPoint.Y))
              {
                  Console.WriteLine("These rectangles have common line [{0};{1}] -> [{2};{3}] ",crossingPoint.X,crossingPoint.Y,crossingThisPoint.X,crossingThisPoint.Y);
                  Point minPoint = crossingPoint.X+crossingPoint.Y < crossingThisPoint.X+ crossingThisPoint.Y ? crossingPoint : crossingThisPoint;
                  return new Rectangle(minPoint,Math.Abs(crossingPoint.Y-crossingThisPoint.Y),Math.Abs(crossingPoint.X-crossingThisPoint.X));
              }
              double x= Math.Min(crossingPoint.X,crossingThisPoint.X);
              double y= Math.Min(crossingPoint.Y,crossingThisPoint.Y);
              double width = Math.Max(crossingPoint.X,crossingThisPoint.X)-x;
              double height = Math.Max(crossingPoint.Y,crossingThisPoint.Y)-y;
              return new Rectangle(new Point(x,y),height,width);
           }
           else
           {
              throw new InvalidOperationException();
           }
        }

        // this method checks if rectangles are crossed and return point at which they are crossing
        public bool IsCrossingPoint(Rectangle rec, out Point point)
        {
            Point[] arr = rec.GetPointsArray();
             point =new Point(0,0);
            foreach (var item in arr)
            {
                if((item.X >= FirstPoint.X && item.X <= FirstPoint.X+Width) &&
                   (item.Y >= FirstPoint.Y && item.Y <= FirstPoint.Y+Height))
                   {
                       point = new Point(item.X,item.Y);
                       return true;
                   }
            }
            return false;
        }
           
       #endregion

    }
    static void Main()
    {
        try
        {
            Rectangle rec = new Rectangle(new Point(1,1),3,3);
            rec.Print();
            rec.MoveAt(new Point(-2,-3));
            rec.Height=2;
            rec.Print();
            Rectangle rec2= new Rectangle(new Point(1,0),1,2);
            rec2.Print();
            rec.MinRectangle(rec2).Print();
            rec.Crossing(rec2);
            
            Rectangle rec1 = new Rectangle(new Point(1, -5),3,2);
            Rectangle rec2 = new Rectangle(new Point(-1,-3),2,3);
            rec1.Crossing(rec2).Print();
            
        }
        catch (InvalidOperationException)
        {
          Console.WriteLine("This exception occurrs when you try to find intersection  of rectangles which haven`t it.  ");
        }     
    }
}