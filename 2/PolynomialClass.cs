using System;
class Task
{
    class Polynomial
    {
      #region Fields
        public int degree{ get;set;}
        private double[] coefficients;
        public double[] Coefficients
        {
            get
            {
               return coefficients;
            }
        }
      #endregion
        
      #region Constructor
        public Polynomial()
        {
            degree =0;
            coefficients = new double[degree+1];
        }

        public Polynomial(int degree,double[] coef)
        {
            this.degree=degree;
            this.coefficients=coef;
        }
      #endregion
 
      #region Methods
          public void Print()
        {
            Console.WriteLine("The polynomial with degree of {0} .",degree);

            Console.Write(coefficients[0]); 
            for ( int i = 1; i < coefficients.Length; i++)
            {
                if(coefficients[i]!=0)
                {
                   Console.Write("  +  ( {0}*x^{1} ) ",coefficients[i],i);
                }
            } 
            Console.WriteLine();               
        }


        public double Calculate(double variable)
        {
           double result =0;
          for (int i = 0; i < coefficients.Length; i++)
           {
               result += coefficients[i]*Math.Pow(variable,i);
               
           }
           return result;
        }

         // function that make all calculation with polynomials using delegate
        private static Polynomial DoOperation(Polynomial p1, Polynomial p2,Func<double,double,double> operation)
        {
           double[] NewCoef= new double[Math.Max(p1.Coefficients.Length,p2.Coefficients.Length)];
           int i=0;
           int last =Math.Min(p1.Coefficients.Length,p2.Coefficients.Length);
           for ( i = 0; i < last; i++)
           {
               NewCoef[i]=operation(p1.Coefficients[i],p2.Coefficients[i]);
           }
           while(i<p1.Coefficients.Length)
           {
               NewCoef[i]=p1.Coefficients[i];
               ++i;
           } 
           while(i<p2.Coefficients.Length)
           {
               NewCoef[i]=p2.Coefficients[i];
               ++i;
           }
           return new Polynomial(Math.Max(p1.degree,p2.degree),NewCoef);
        }

        public static Polynomial operator + (Polynomial p1, Polynomial p2)
        {
           return DoOperation(p1,p2,(x,y) => x+y);
        }

        public static Polynomial operator - (Polynomial p1, Polynomial p2)
        {
           return DoOperation(p1,p2,(x,y) => x-y);
        }

        public static Polynomial operator * (Polynomial p1, Polynomial p2)
        {
            double[] mas = new double[p1.degree+p2.degree+1];
           for (int i = 0; i < p1.Coefficients.Length; i++)
           {
               for (int j = 0; j < p2.Coefficients.Length; j++)
               {
                   mas[i+j] += p1.Coefficients[i]*p2.Coefficients[j];
               }
           }
           return new Polynomial(p1.degree+p2.degree,mas);
        }
      #endregion
        

    }
    static void Main()
    {
        //               x^0  x^1    x^2   x^3
        double[] coef = {1.0, -2.0 , 0.0 , 4.0};
        Polynomial p1 = new Polynomial(3,coef);
        p1.Print();

        //               x^0  x^1    x^2  
        double[] coef2 = {5.0, 1.0,  5.0};
        Polynomial p2 = new Polynomial(2,coef2);
        p2.Print();
        
        Polynomial p3 = p1*p2;
        p3.Print();

        double variable =1.0;
        Console.WriteLine("Variable value:{0}. Polynomial result {1}.",variable,p1.Calculate(variable));

    }
}