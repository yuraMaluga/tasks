class KRangeMinor:Minor
{
    private int[] RowIndex;
    private int[] ColumnIndex;
    public KRangeMinor(double[,] matrix, int height, int width,int[] rowIndex,  int[] columnIndex):base(matrix,height,width)
    {
       RowIndex=rowIndex;
       ColumnIndex=columnIndex;
    }
    public override double Calculate()
    {
        double[,] minor = new double[RowIndex.Length, ColumnIndex.Length];          
                for (int i = 0; i < RowIndex.Length; i++)
                {
                    for (int j = 0; j < ColumnIndex.Length; j++)
                    {
                        minor[i,j] = Matrix[RowIndex[i], ColumnIndex[j]];
                    }
                }

                return DetCalculation(minor);
    }
}