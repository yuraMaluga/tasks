using System;
using System.Collections.Generic;
abstract class Minor
{
   public double[,] Matrix { get; set; }
   public int Height { get; set; }
   public int Width { get; set; }

        public Minor(double[,] matrix, int height, int width)
        {
                Matrix = matrix;
                Height = height;
                Width = width;
            }

        

        protected void SimplifyMatrix(ref double[,] mtr, int currentI, int currentJ)
        {
            for (int i = currentI + 1; i < Math.Sqrt(mtr.Length); i++)
            {
                    double k=0;
                    if (mtr[i, currentJ] != 0)
                    {
                         k = -mtr[i, currentJ]/ mtr[currentI, currentJ] ;
                    }
                for (int j = currentJ; j < Math.Sqrt(mtr.Length); j++)
                {
                        if(k==0)
                        {
                            break;
                        }
                        mtr[i, j] = mtr[i, j] + mtr[currentI,j]*k;
                }
            }
        }

        protected double DetCalculation(double[,] mtr)
        {
            for (int j = 0; j < Math.Sqrt(mtr.Length) - 1; j++)
            {            
                 SimplifyMatrix(ref mtr, j, j);         
            }
                double res = 1;
                for (int i = 0; i < Math.Sqrt(mtr.Length); i++)
                {
                    res *= mtr[i, i];
                }
            return res ;
        }
        public void Print()
        {
            Console.WriteLine("Matrix");
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.Write("{0}   ", Matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
        public abstract double Calculate();
        public static double operator+(Minor m1, Minor m2)
        {
            return m1.Calculate()+m2.Calculate();
        }
        public static double operator*(Minor m1, Minor m2)
        {
            return m1.Calculate()*m2.Calculate();
        }
        public static double operator/(Minor m1, Minor m2)
        {
            return m1.Calculate()/m2.Calculate();
        }

}
class ExtendedMinor:Minor
{
    private int  k;
    public ExtendedMinor(double[,] matrix, int height, int width,int k):base(matrix,height,width)
    {
        this.k =k;
    }
    public override double Calculate()
    {
        double[,] minor = new double[Height-k, Width-k];
                int mI = 0;
                int mJ = 0;
                for (int i = k; i < Height; i++)
                {
                    for (int j = k; j < Width; j++)
                    {
                        minor[mI, mJ] = Matrix[i, j];
                        ++mJ;
                    }
                    mJ = 0;
                    ++mI;
                }
                return DetCalculation(minor);
    }

}
class KRangeMinor:Minor
{
    private int[] RowIndex;
    private int[] ColumnIndex;
    public KRangeMinor(double[,] matrix, int height, int width,int[] rowIndex,  int[] columnIndex):base(matrix,height,width)
    {
       RowIndex=rowIndex;
       ColumnIndex=columnIndex;
    }
    public override double Calculate()
    {
        double[,] minor = new double[RowIndex.Length, ColumnIndex.Length];          
                for (int i = 0; i < RowIndex.Length; i++)
                {
                    for (int j = 0; j < ColumnIndex.Length; j++)
                    {
                        minor[i,j] = Matrix[RowIndex[i], ColumnIndex[j]];
                    }
                }

                return DetCalculation(minor);
    }
}
class MinorOfIJ:Minor
{
    private int minorI;
    private int minorJ;

    public MinorOfIJ(double[,] matrix, int height, int width,int minorI, int minorJ):base(matrix,height,width)
    {
       this.minorI=minorI;
       this.minorJ=minorJ;
    }
     public override double Calculate()
    {
        double[,] minor = new double[Height - 1, Width - 1];
                int mI = 0;
                int mJ = 0;
                for (int i = 0; i < Height; i++)
                {
                    if(i!=minorI)
                    {                    
                        for (int j = 0; j < Width; j++)
                        {
                            if(j!= minorJ)
                            {
                                minor[mI, mJ] = Matrix[i, j];
                                ++mJ;
                            }
                        }
                        mJ = 0;
                        ++mI;
                    }
                }
                return DetCalculation(minor);
    }

}