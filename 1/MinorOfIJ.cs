class MinorOfIJ:Minor
{
    private int minorI;
    private int minorJ;

    public MinorOfIJ(double[,] matrix, int height, int width,int minorI, int minorJ):base(matrix,height,width)
    {
       this.minorI=minorI;
       this.minorJ=minorJ;
    }
     public override double Calculate()
    {
        double[,] minor = new double[Height - 1, Width - 1];
                int mI = 0;
                int mJ = 0;
                for (int i = 0; i < Height; i++)
                {
                    if(i!=minorI)
                    {                    
                        for (int j = 0; j < Width; j++)
                        {
                            if(j!= minorJ)
                            {
                                minor[mI, mJ] = Matrix[i, j];
                                ++mJ;
                            }
                        }
                        mJ = 0;
                        ++mI;
                    }
                }
                return DetCalculation(minor);
    }

}