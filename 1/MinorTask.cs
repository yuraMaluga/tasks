using System;
class Task1
{
    static void Main()
    {
        double[,] matrix =
        {
            {3,4,-9,5},
            {-15,-12,50,-16},
            {-27,-36,73,8},
            {9,12,-10,-16}
        };
        ExtendedMinor exMinor = new ExtendedMinor(matrix,4,4,2);
        exMinor.Print();

        Console.WriteLine(exMinor.Calculate());
        MinorOfIJ ijMinor = new MinorOfIJ(matrix,4,4,1,2);
        Console.WriteLine(ijMinor.Calculate());

       
        KRangeMinor kMinor= new KRangeMinor(matrix,4,4,new int[]{1,2,3}, new int[]{1,2,3});
        Console.WriteLine(kMinor.Calculate());

        Console.WriteLine("{0} + {1} ={2} ",exMinor,ijMinor,exMinor+ijMinor);
       
    }
}