class ExtendedMinor:Minor
{
    private int  k;
    public ExtendedMinor(double[,] matrix, int height, int width,int k):base(matrix,height,width)
    {
        this.k =k;
    }
    public override double Calculate()
    {
        double[,] minor = new double[Height-k, Width-k];
                int mI = 0;
                int mJ = 0;
                for (int i = k; i < Height; i++)
                {
                    for (int j = k; j < Width; j++)
                    {
                        minor[mI, mJ] = Matrix[i, j];
                        ++mJ;
                    }
                    mJ = 0;
                    ++mI;
                }
                return DetCalculation(minor);
    }

}