using System;
using System.Linq;

class Task3
{
    class VectorSizeIncompatibilityException:Exception
    {
       public VectorSizeIncompatibilityException()
       {
           Console.WriteLine("Happened VectorSizeIncompatibilityException.");
       }
    }
    class VectorDataIncompatibilityException:Exception
    {
          public VectorDataIncompatibilityException()
       {
           Console.WriteLine("Happened VectorDataIncompatibilityException.");
       }
    }
    class TernaryVector
    {
        #region Fields

        public int Size{get;set;}
        private int[] array;
        public int[] Array
        {
            get
            {
               return array;
            }
        }   

        #endregion

        #region Constructors
            
        public TernaryVector()
        {
           this.Size=0;
           array= new int[Size];
        }
        public TernaryVector(int size)
        {
           this.Size=size;
           VectorGenerate();
        }

        public TernaryVector(params int[] numbers)
        {
           if(CorrectData(numbers))
           {
               array=numbers;
               Size=numbers.Length;
           }
           else
           {
               throw new VectorDataIncompatibilityException();
           }
        }

        #endregion

        #region Methods
            
        public bool IsOrtogonalVector(TernaryVector tVector)
        {
           int sum =0;
           if(Size != tVector.Size)
           {
               throw new VectorSizeIncompatibilityException();
           }
           else
           {
               for (int i = 0; i < Size; i++)
               {
                   sum += tVector.Array[i]*array[i];
               }
           }
           return sum == 0;
        }

        public int VariableQuantity(int v)
        {
          return array.Count(n=> n== v);
        }
   
        public TernaryVector Combine(TernaryVector v)
        {
            if(Size!=v.Size)
            {
                throw new VectorSizeIncompatibilityException();
            }
            else
            {
                if(!IsOrtogonalVector(v))
                {
                    int[]arr= new int[array.Length];
                    for (int i = 0; i < Size; i++)
                    {
                        arr[i] =array[i]<v.Array[i] ? array[i] : v.Array[i];
                    }
                    return new TernaryVector(arr);
                }
                else
                {
                    Console.WriteLine("This operation is incorrect , vectors are ortogonal");
                    return new TernaryVector();
                }
            }
        }
        
        public void Print()
        {
            if(array.Length==0)
            {
                Console.WriteLine("Vector is empty");
            }
            else
            {
                Console.WriteLine("Vector:");
                foreach (var item in array)
                {
                    Console.Write("{0} -> ",item);
                }
                Console.WriteLine();    
            }
        }
     
       // genrate vector automatically
        private void VectorGenerate()
        {
            Random rand = new Random();
            array = new int[Size];
            for (int i = 0; i < Size; i++)
            {
                array[i] =rand.Next(0,3);
            }
        }
        // check if all number is from set {0,1,2}
        private bool CorrectData(int[] mas)
        {
            int n = mas.Count(i => i<0 || i>2);
            return n ==0 ;               
        }
        
        #endregion


        
    }
    static void  Main()
    {
        try
        {
            // vector will be generated automatically
            TernaryVector vector1 = new TernaryVector(7); 
            vector1.Print();

            int[] array = {0,2,1,0,2,1,0};
        
            TernaryVector vector2 = new TernaryVector(array);
            vector2.Print();
            Console.WriteLine("The item {0} occered in vector2 {1} times.",2,vector2.VariableQuantity(2));
       
            TernaryVector vector3 = vector1.Combine(vector2);
            vector3.Print();

        }
        catch (VectorDataIncompatibilityException)
        {
             Console.WriteLine("This exception occurred because vectors item isn`t correct.");
        }
        catch (VectorSizeIncompatibilityException)
        {
             Console.WriteLine("This exception occurred because vectors capacity is different");
        }   
        

    }
}